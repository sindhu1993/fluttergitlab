import 'dart:convert';

import 'package:flutter_booking_servis_hook_riverpod/models/BannerModel.dart';
import 'package:flutter_booking_servis_hook_riverpod/network/base_url.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:http/http.dart' as http;


class ApiRepo{

  // List Banner
  Future<BannerModel> fetchBanner() async{
    final response = await http.get(Uri.parse(Api.baseUrl+"list_banner"));
    if(response.statusCode == 200){
      var res = json.decode(response.body);
      return BannerModel.fromJson(res[0]); 

    }else{
         throw Exception('gak dapet data');
    }
  }
  // List Banner
}

final apiProvider = Provider((_) => new ApiRepo());
final airData = FutureProvider<BannerModel>((ref){
  final api = ref.watch(apiProvider);
  return api.fetchBanner();
});