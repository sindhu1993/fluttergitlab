import 'dart:convert';

import 'package:flutter/material.dart';
import '../screens/master_menu.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../network/base_url.dart';
import 'package:sweetalert/sweetalert.dart';
import 'package:http/http.dart' as http;

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController username = new TextEditingController();
  TextEditingController password = new TextEditingController();

  // function
  bool _secureText = true;
  String usernameParam;
  String idCustomerParam;

  showHide() {
    setState(() {
      _secureText = !_secureText;
    });
  }

  _checkLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      usernameParam = prefs.getString('username');
      idCustomerParam = prefs.getString('id_customer');

      if (idCustomerParam != null) {}
    });
  }

  addSharePref(String idCustomer, String username) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setString('id_customer', idCustomer);
      prefs.setString('username', username);
    });
  }

  _login() async {
    if (username.text.isEmpty || password.text.isEmpty) {
      SweetAlert.show(context,
          title: 'Perhatian..',
          subtitle: 'Username dan Password Harus Diisi',
          style: SweetAlertStyle.error,
          showCancelButton: false,
          onPress: (bool isConfirm) {
            if (isConfirm) {
              Navigator.of(context, rootNavigator: true).pop('dialog');
              // return false;
            }
          });
    } else {
      final response = await http.post(Uri.parse(Api.baseUrl + "login"),
          body: {"username": username.text, "password": password.text});

      var dataUser = json.decode(response.body);

      if (dataUser.length == 0) {
        setState(() {
          SweetAlert.show(context,
              title: 'Login Gagal',
              subtitle: 'Username atau Password Salah',
              style: SweetAlertStyle.error,
              showCancelButton: false, onPress: (bool isConfirm) {
            if (isConfirm) {
              Navigator.of(context, rootNavigator: true).pop('dialog');
              // return false;
            }
          });
        });
      } else {
        setState(() {
          usernameParam = dataUser[0]['username'];
          idCustomerParam = dataUser[0]['id_customer'];
          addSharePref(idCustomerParam, usernameParam);
        });
        SweetAlert.show(context,
            title: "Sukses Login",
            subtitle: "Klik Ok untuk melanjutkan",
            style: SweetAlertStyle.success,
            showCancelButton: false, onPress: (bool isConfirm) {
          if (isConfirm) {
            Navigator.of(context, rootNavigator: true).pop('dialog');
            Navigator.pop(context);
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => new MasterMenu()));
            return false;
          }
        });
      }
    }
  }

  @override
  void initState() {
    super.initState();
    _checkLogin();
  }
  // function

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Column(
        children: [
          _loginWidget(context),
        ],
      ),
    ));
  }

  // widget Loading
  _loadingWidget() {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(
              margin: EdgeInsets.only(left: 7), child: Text("Loading...")),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  // widget Loading

  // widget Login
  Widget _loginWidget(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 40.0),
          child: Center(
            child: Container(
                width: MediaQuery.of(context).size.width * 0.5,
                height: MediaQuery.of(context).size.height * 0.4,
                child: Image.asset(
                  'assets/img/logo.png',
                )),
          ),
        ),
        Padding(
          //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: TextFormField(
            controller: username,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Username',
                hintText: 'Enter valid username'),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(
              left: 15.0, right: 15.0, top: 15, bottom: 0),
          //padding: EdgeInsets.symmetric(horizontal: 15),
          child: TextFormField(
            controller: password,
            obscureText: _secureText,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Password',
                hintText: 'Enter secure password',
                suffixIcon: IconButton(
                    icon: Icon(
                        _secureText ? Icons.visibility : Icons.visibility_off),
                    onPressed: showHide)),
          ),
        ),
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.035,
        ),
        Container(
          height: MediaQuery.of(context).size.height * 0.1,
          width: MediaQuery.of(context).size.width * 0.9,
          decoration: BoxDecoration(
              color: Colors.blue, borderRadius: BorderRadius.circular(30)),
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(30.0)),
            ),
            onPressed: () {
              _loadingWidget();
              _login();
            },
            child: Text(
              'Login',
              style: TextStyle(color: Colors.white, fontSize: 25),
            ),
          ),
        ),
      ],
    );
  }
  // widget Login

}
