import 'package:flutter/material.dart';
import 'package:flutter_booking_servis_hook_riverpod/providers/provider.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../network/base_url.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 10, top: 10, bottom: 5),
                  child: Text(
                    'Banner',
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
                  ),
                ),
              ],
            ),
            BannerWidget(),
          ],
        ),
      ),
    );
  }
}

class BannerWidget extends ConsumerWidget {
  @override
  Widget build(BuildContext context, watch) {
    final banner = watch(airData);
    return RefreshIndicator(
      onRefresh: () async {
        context.refresh(airData);
      },
      child: SizedBox(
        height: MediaQuery.of(context).size.height,
        width: 100,
        child: new ListView(
          children: [
            banner.when(
                data: (data) => Text(data.tglPost),
                loading: () => Center(child: CircularProgressIndicator()),
                error: (e, s) => Text('$e')),
          ],
        ),
      ),
    );
  }
}
