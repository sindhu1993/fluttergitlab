import 'package:flutter/material.dart';
import 'package:flutter_booking_servis_hook_riverpod/screens/BookingScreen.dart';
import '../screens/home_screen.dart';
import '../screens/login_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MasterMenu extends StatefulWidget {
  @override
  _MasterMenuState createState() => _MasterMenuState();
}

class _MasterMenuState extends State<MasterMenu> {
  String usernameParam;
  String idCustomer;
  int selectIndex = 0;

  // function
  @override
  void initState() {
    super.initState();
    getSharePref();
  }

  getSharePref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      usernameParam = prefs.getString('username');
      idCustomer = prefs.getString('id_customer');
    });

    if (usernameParam == "" && idCustomer == "") {
      Navigator.of(context).pop();
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => new LoginScreen()));
    }
  }
  // function

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Offstage(
            offstage: selectIndex != 0,
            child: TickerMode(
              enabled: selectIndex == 0,
              child: HomeScreen(),
            ),
          ),
          Offstage(
            offstage: selectIndex != 1,
            child: TickerMode(
              enabled: selectIndex == 1,
              child: BookingScreen(),
            ),
          ),
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        color: Color.fromRGBO(255, 255, 255, 1),
        child: Container(
          height: 60,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              InkWell(
                onTap: () {
                  setState(() {
                    selectIndex = 0;
                  });
                },
                child: Column(
                  children: [
                    SizedBox(height: 10),
                    Icon(
                      Icons.home,
                      color: (selectIndex == 0)
                          ? Color.fromRGBO(62, 122, 250, 1)
                          : Color.fromRGBO(185, 185, 185, 1),
                    ),
                    Text(
                      'Beranda',
                      style: TextStyle(
                          color: (selectIndex == 0)
                              ? Color.fromRGBO(62, 122, 250, 1)
                              : Color.fromRGBO(185, 185, 185, 1)),
                    ),
                  ],
                ),
              ),

              InkWell(
                onTap: () {
                  setState(() {
                    selectIndex = 1;
                  });
                },
                child: Column(
                  children: [
                    SizedBox(height: 10),
                    Icon(
                      Icons.home,
                      color: (selectIndex == 1)
                          ? Color.fromRGBO(62, 122, 250, 1)
                          : Color.fromRGBO(185, 185, 185, 1),
                    ),
                    Text(
                      'Booking',
                      style: TextStyle(
                          color: (selectIndex == 1)
                              ? Color.fromRGBO(62, 122, 250, 1)
                              : Color.fromRGBO(185, 185, 185, 1)),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
