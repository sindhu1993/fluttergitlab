import 'dart:convert';

import 'package:flutter_booking_servis_hook_riverpod/models/BannerModel.dart';
import 'package:flutter_booking_servis_hook_riverpod/network/base_url.dart';
import 'package:http/http.dart' as http;

class Repository{
  Future<BannerModel> getBanner(context) async {
  BannerModel result;
  try {
    final response = await http.get(Uri.parse(Api.baseUrl+"list_banner"));
    if (response.statusCode == 200) {
      final item = json.decode(response.body);
      result = BannerModel.fromJson(item);
    } else {
      // return Text('sasa');
    }
  } catch (e) {
    // log(e);
  }
  return result;
  }
}