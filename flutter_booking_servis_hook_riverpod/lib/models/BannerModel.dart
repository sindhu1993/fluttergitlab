// class BannerModel {
//   final String judul;
//   final String isi;
//   final String gambar;
//   final String tgl_post;

//   BannerModel({this.judul,  this.isi, this.gambar,
//   this.tgl_post});

//   factory BannerModel.fromJson(Map<String, dynamic> json){
//     return BannerModel(
//       judul: json['judul'],
//       isi: json['isi'],
//       gambar: json['gambar'],
//       tgl_post: json['tgl_post']
//     );
//   }
// }
// 
// 

// To parse this JSON data, do

    // final bannerModel = bannerModelFromJson(jsonString);

import 'dart:convert';

List<BannerModel> bannerModelFromJson(String str) => List<BannerModel>.from(json.decode(str).map((x) => BannerModel.fromJson(x)));

String bannerModelToJson(List<BannerModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class BannerModel {
    BannerModel({
        this.judul,
        this.isi,
        this.gambar,
        this.tglPost,
    });

    String judul;
    String isi;
    String gambar;
    String tglPost;

    factory BannerModel.fromJson(Map<String, dynamic> json) => BannerModel(
        judul: json["judul"] == null ? null : json["judul"],
        isi: json["isi"] == null ? null : json["isi"],
        gambar: json["gambar"] == null ? null : json["gambar"],
        tglPost: json["tgl_post"] == null ? null : json["tgl_post"],
    );

    Map<String, dynamic> toJson() => {
        "judul": judul == null ? null : judul,
        "isi": isi == null ? null : isi,
        "gambar": gambar == null ? null : gambar,
        "tgl_post": tglPost == null ? null : tglPost,
    };
}
